from flask import Flask, request

app = Flask(__name__)

class domain:
    def index(self):
        return 'hello bob from user103'
    def add(self, a, b):
        return str(int(a)+int(b))
    def sub(self, a, b):
        return str(int(a)-int(b))
  
_domain = domain()
  
@app.route('/', methods=['GET'])
def index():
    return _domain.index()

@app.route('/add', methods=['GET'])
def add():
    return _domain.add(request.args.get('a'),int(request.args.get('b')))
   
@app.route('/sub', methods=['GET'])
def sub():
    return _domain.sub(request.args.get('a'),int(request.args.get('b')))


if __name__ == '__main__':  
    app.run('0.0.0.0',port=8103)
