import unittest
import app

class Domain_Test(unittest.TestCase):
    def test_home(self):
        instance = app.domain()
        result = instance.index()
        self.assertEqual(result,'hello bob from user103')

    def test_add(self):
        instance = app.domain()
        result = instance.add(1,2)
        self.assertEqual(int(result), 3)
        
    def test_sub(self):
        instance = app.domain()
        result = instance.sub(1,2)
        self.assertEqual(int(result), -1)

if __name__ == "__main__":
    unittest.main()
